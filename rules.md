# Possible rules for how KOO board might work

This documents the rules governing the KOO board. It discusses how
some other projects do this kind of thing, and then makes a concrete
proposal.

## Debian Enhancement Proposal

* <https://dep-team.pages.debian.net/>
* each proposal has one or more drivers
* proposal process as a state machine: DRAFT, CANDIDATE, ACCEPTED,
  OBSOLETE, REJECTED
* proposals are tracked on the Debian git server
  - <https://salsa.debian.org/dep-team/deps>
* highly consensus based, no voting method, but process is shepherded
  by the DEP0 drivers
* existing proposals are partly technical (e.g., machine readable
  debian/copyright files), partly process (handling non-maintainer
  uploads)
* started in 2007, now sixteen proposals, not a huge success, but at
  least it's lightweight

# OpenStreetMap Foundation

* <https://wiki.osmfoundation.org/wiki/Board_Rules_of_Order>
* OSMF is more similar to KOO than Debian is
* rules are not too long, and are pretty clear
* I like the quorum definition (50% of board present, simple majority
  among those present), with meeting agendas announced a week ahead of
  time

# Comments

* The OSMF rules seem pretty good and mostly applicable to KOO; we
  should adopt them as a first working draft
* A voting mechanism needs to be chosen. It should be kept simple,
  especially from a technical point of view.
  - I would not recommend the software Debian uses (devotee), as it's
    surprisingly hard to use, even for a voter
  - real-time votes (in person or over a video chat), or ballots as
    signed emails, or signed git commits would probably work better
* Keeping track of decisions seems important for transparency and
  trust.
